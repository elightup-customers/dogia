<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

add_action ( 'after_setup_theme', 'themes_child_setup' );
function themes_child_setup() {
	add_image_size( 'img-post-big', 510, 375, true );
	add_image_size( 'img-post-small', 265, 187, true );
	add_image_size( 'img-product-small', 317, 211, true );
	add_image_size( 'img-post-home', 300, 300, true );
	add_image_size( 'img-post-max', 660, 490, true );
	//add_image_size( 'img-tin-hot', 186, 132, true );
	add_image_size( 'img-dai-trang', 537, 400, true );
	add_image_size( 'img-dai-trang-dai', 236, 400, true );
	register_nav_menus(
		array(
			'daily-nav' => 'Menu đại lý',
			'category-dai-ly' => 'Chuyên mục đại lý',
			'category-menu' => 'Menu chuyên mục'
		)
	);

}
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

/*
 * Redirect user dai-ly to homepage.
 */
// function redirect_daily( $url, $request, $user ){
// 	$user_role = get_user_meta( $user->ID, 'wp_capabilities' );
// 	var_dump($user_role);
// 	if ( 'dai_ly' === $user_role ) {
// 		$url = home_url();
// 	}
// 	return $url;
// }
// add_filter( 'login_redirect', 'redirect_daily', 10, 3 );


// add_action( 'admin_init', 'redirect_daily_role_admin_users' );
// function redirect_daily_role_admin_users() {
// 	$current_user = wp_get_current_user();
// 	if ( 'dai_ly' === $current_user->roles[0] ) {
// 		wp_redirect( home_url() );
// 		exit;
// 	}
// }

/*
 * Check menu danh mục when dai_ly roles.
 */
function show_menu_danh_muc_daily() {
	ob_start();
	$current_user = wp_get_current_user();
	if ( 'dai_ly' === $current_user->roles[0] ) {
		wp_nav_menu( array(
			 'theme_location' => 'category-dai-ly', // tên location cần hiển thị
			 'container' => 'nav', // thẻ container của menu
			 'container_class' => 'second-nav', //class của container
			 'menu_class' => 'menu danh-muc-bottom' // class của menu bên trong
		) );
	} else {
		wp_nav_menu( array(
			 'theme_location' => 'category-menu', // tên location cần hiển thị
			 'container' => 'nav', // thẻ container của menu
			 'container_class' => 'second-nav', //class của container
			 'menu_class' => 'menu danh-muc-bottom' // class của menu bên trong
		) );
	}
	return ob_get_clean();
}
add_shortcode( 'show-menu-danh-muc', 'show_menu_danh_muc_daily' );

/*
 * Check menu when dai_ly roles.
 */
function show_menu_daily() {
	ob_start();
	$current_user = wp_get_current_user();
	if ( 'dai_ly' === $current_user->roles[0] ) {
		wp_nav_menu( array(
			'theme_location' => 'daily-nav', // tên location cần hiển thị
			'container' => 'nav', // thẻ container của menu
			'container_class' => 'second-nav', //class của container
			'menu_class' => 'menu clearfix' // class của menu bên trong
		) );
	} else {
		wp_nav_menu( array(
			'theme_location' => 'header',
		) );
	}
	return ob_get_clean();
}
add_shortcode( 'show-menu', 'show_menu_daily' );

/*
 * Remove addmin bar when dai_ly roles.
 */
function hide_admin_bar(){
	$current_user = wp_get_current_user();
	if ( 'dai_ly' === $current_user->roles[0] ) {
		return false;
	} elseif ('administrator' === $current_user->roles[0]) {
		return true;
	}
}
add_filter( 'show_admin_bar', 'hide_admin_bar' );

/*
 * Add shortcode
 */
function add_shortcode_atc() {
	ob_start();
	ELUSHOP\Cart::add_cart();
	return ob_get_clean();
}
add_shortcode( 'add_to_cart', 'add_shortcode_atc' );

function add_shortcode_cart() {
	ob_start();
	ELUSHOP\Cart::cart();
	return ob_get_clean();
}
add_shortcode( 'mua_ngay', 'add_shortcode_cart' );

function shortcode_price() {
	ob_start();
	$sp_id = get_the_ID();
	$price = get_post_meta( $sp_id, 'price', true );
	echo number_format( $price, 0, '', '.' ) . '<sup>₫</sup>';
	return ob_get_clean();
}
add_shortcode( 'price', 'shortcode_price' );

function shortcode_price_before_sale() {
	ob_start();
	$sp_id = get_the_ID();
	$price_sale = get_post_meta( $sp_id, 'price_before_sale', true );
	if ( !empty( $price_sale ) ) {
		echo '</br>' . number_format( $price_sale, 0, '', '.' ) . '<sup>₫</sup>';
	}
	return ob_get_clean();
}
add_shortcode( 'price_before_sale', 'shortcode_price_before_sale' );

// function percent() {
// 	ob_start();
// 	$sp_id = get_the_ID();
// 	$price_sale = get_post_meta( $sp_id, 'price_before_sale', true );
// 	$price = get_post_meta( $sp_id, 'price', true );

// 	if ( !empty( $price_sale ) ) {
// 		$percent = ( $price / $price_sale ) * 100;
// 		echo'<div class="top-product sale-title sale-hide">
// 			<span>Sale</span>
// 			</div>
// 			<div class="top-product percent-count sale-hide">
// 				<span>' . '<sup>- </sup>' . number_format( $percent, 0, '', '.' ) . '<sup>%</sup>' . '</span>
// 			</div>';
// 	}
// 	return ob_get_clean();
// }
// add_shortcode( 'percent', 'percent' );

function shortcode_gallery_slide() {
	ob_start();
	$sp_id = get_the_ID();
	$gallery_images = get_post_meta( $sp_id, 'replated_image' );
	$feature_images = get_the_post_thumbnail( $sp_id, $size = 'product-image', [
						'class' => 'img-zoom',
						'data-zoom-image'   => wp_get_attachment_url( get_post_thumbnail_id($sp_id, 'full') ),
					] );
	echo '<div class="slider-gallery">';
	if ( $gallery_images ) {
		foreach ( $gallery_images as $key => $gallery_image ) {
			if ( $key >= 0 ) {
				$intro_image = wp_get_attachment_image_src( $gallery_image, 'product-image' );
				$intro_image_zoom = wp_get_attachment_image_src( $gallery_image, 'full' );
				if ( ! empty( $intro_image[0] ) ) {
					echo '<div class="slider-gallery-item"><img class="img-zoom" data-zoom-image="' . esc_url( $intro_image_zoom[0] ) . '" src="' . esc_url( $intro_image[0] ) . '"/></div>';
				}
			}
		}
	} else {
		echo '<div class="slider-gallery-item">' . $feature_images . '</div>';
	}
	echo '</div>';
	return ob_get_clean();
}
add_shortcode( 'gallery_images', 'shortcode_gallery_slide' );

// Shortcode Excerpt.
function shortcode_excerpt() {
	ob_start();
	if ( has_excerpt() ) {
		$description = get_the_excerpt();
		echo '<p>' . $description . '</p>';
	}
	return ob_get_clean();
}
add_shortcode( 'excerpt', 'shortcode_excerpt' );

function shortcode_reviews() {
	ob_start();
	$sp_id = get_the_ID();
	$reviews = get_post_meta( $sp_id, 'reviews', true );
	echo '<div class="color-gold">';
	for ( $i = 0; $i < 5; $i++ ) {
		if ( $i < $reviews ) {
			echo '<i class="fas fa-star"></i>';
		} else {
			echo '<i class="far fa-star"></i>';
		}
	}
	echo '</div>';
	return ob_get_clean();
}
add_shortcode( 'reviews', 'shortcode_reviews' );

function shortcode_comments() {
	ob_start();
	// var_dump(comments_template( 'comments.php' ));
	if ( is_user_logged_in() || get_comments_number() && is_user_logged_in() ) {
		comments_template( 'comments.php' );
	} else {
		echo esc_html__( 'Bạn vui lòng đăng nhập để có thể đánh giá được bài sản phẩm', 'fl' );
	}
	return ob_get_clean();
}
add_shortcode( 'comments', 'shortcode_comments' );

// Lấy ảnh đại diện cho bài post ở page tin tức size lớn
function show_img_post() {
	ob_start();
	echo the_post_thumbnail('img-post-big');
	return ob_get_clean();
}
add_shortcode( 'show-img-post', 'show_img_post' );

// Lấy ảnh đại diện cho bài post ở page tin tức size nhỏ
function show_img_small() {
	ob_start();
	echo the_post_thumbnail('img-post-small');
	return ob_get_clean();
}
add_shortcode( 'show-img-post-small', 'show_img_small' );

// Lấy ảnh đại diện cho sản phẩm ở home page.
function do_gia_img_product() {
	ob_start();
	echo '<a href="' . get_the_permalink() . '">' . get_the_post_thumbnail( get_the_ID(),'img-dai-trang') . '</a>';
	return ob_get_clean();
}
add_shortcode( 'img-product', 'do_gia_img_product' );

// Lấy ảnh đại diện cho bài post ở page tin tức size nhỏ
function do_gia_img_post_home() {
	ob_start();
	echo the_post_thumbnail('img-product-small');
	return ob_get_clean();
}
add_shortcode( 'post-home', 'do_gia_img_post_home' );

// Lấy ảnh đại diện cho bài post ở page tin tức size dài
function do_gia_img_dai_trang_dai() {
	ob_start();
	echo the_post_thumbnail('img-dai-trang-dai');
	return ob_get_clean();
}
add_shortcode( 'post-long', 'do_gia_img_dai_trang_dai' );

// Lấy ảnh đại diện cho bài post đại tràng lớn
function do_gia_img_dai_trang() {
	ob_start();
	echo the_post_thumbnail('img-dai-trang');
	return ob_get_clean();
}
add_shortcode( 'post-dai-trang', 'do_gia_img_dai_trang' );

// Lấy ảnh đại diện cho bài post ở page tin tức size max
function do_gia_img_post_max() {
	ob_start();
	echo the_post_thumbnail('img-post-max');
	return ob_get_clean();
}
add_shortcode( 'post-max', 'do_gia_img_post_max' );

//Lấy ảnh đại diện cho bài post tin hot
// function do_gia_img_tin_hot() {
// 	ob_start();
// 	echo the_post_thumbnail('img-tin-hot');
// 	return ob_get_clean();
// }
// add_shortcode( 'post-tin-hot', 'do_gia_img_tin_hot' );

//add menu khi đại lý đăng nhập
// add_theme_support( 'menus' );

// Lấy danh mục bài viết
function danh_muc_bài_viet() {
	ob_start();

	$args = array(
    	'type'      => 'post',
    	'child_of'  => 0,
    	'parent'    => '',
		'hide_empty'=> 1
	);
	echo '<ul class="danh-muc-bottom">';
	$categories = get_categories( $args );
	foreach ( $categories as $category ) {
     	echo '<li>';
		   echo '<a href=' . get_term_link($category->slug, 'category') . ' > ' ;
			   echo $category->name;
		   echo '</a>';
		echo '</li>';
	 }
	echo '</ul>';

	return ob_get_clean();
}
add_shortcode( 'danh-muc', 'danh_muc_bài_viet' );

// Menu bottom mobile
function menu_bottom_mobile() {
	ob_start();

	echo '
	<div class="menu-bottom">
		<ul>
			<li><a href="" class="btn-share"><i class="fas fa-share-alt"></i></a>
				<ul class="sub-menu-bottom">
					<li>
						<a id="share-fb" class="share-fb" href="https://www.facebook.com/sharer/sharer.php?u='. get_the_permalink() .' "><i class="fab fa-facebook"></i> Chia sẻ</a>
					</li>
					<li>
						<script src="https://sp.zalo.me/plugins/sdk.js"></script>
						<div class="zalo-share-button" data-href="" data-oaid="579745863508352884" data-layout="1" data-color="blue" data-customize=false></div>
					</li>
				</ul>
			</li>
			<li><a href="tel:0971885885">
				<div class="wrapper">
					<div class="ring">
						<div class="coccoc-alo-phone coccoc-alo-green coccoc-alo-show">
							<div class="coccoc-alo-ph-circle"></div>
							<div class="coccoc-alo-ph-circle-fill"></div>
							<div class="coccoc-alo-ph-img-circle"></div>
						</div>
					</div>
				</div>
			</a></li>
			<li><a href="https://zalo.me/721941227024414381"><img src="http://dogia.vn/wp-content/uploads/2020/04/logo-zalo.png"/></a></li>
			<li><a href="#" class="btn-menu-bottom"><i class="fas fa-bars"></i></a></li>
		</ul>
	</div>
	';
	echo '<div class="menu-bottom-mobile">';
	echo do_shortcode('[show-menu-danh-muc]');
	echo '</div>';
	return ob_get_clean();
}
add_shortcode( 'menu-bottom', 'menu_bottom_mobile' );

// Thêm loại tiền tệ vnđ vào gravity form.
add_filter( 'gform_currencies', 'add_vnd_currency' );
function add_vnd_currency( $currencies ) {
    $currencies['VND'] = array(
        'name'               => __( 'Viet Nam dong', 'gravityforms' ),
        'symbol_left'        => '',
        'symbol_right'       => 'đ',
        'symbol_padding'     => ' ',
        'thousand_separator' => ',',
        'decimal_separator'  => '.',
        'decimals'           => 0
    );

    return $currencies;
}

// Remove unneeded scripts and styles in Beaver Builder.
add_action( 'wp_enqueue_scripts', function() {
	wp_dequeue_style( 'jquery-magnificpopup' );
	wp_dequeue_script( 'jquery-magnificpopup' );
}, 9999 );