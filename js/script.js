jQuery( function ( $ ) {
	'use strict';

	/**
	 * Single-sanpham image slider.
	 */
	function sanphamImageSlider() {
		$( '.slider-gallery' ).slick( {
			autoplay: false,
			arrows: true,
			dots: false,
			slidesToShow: 1,
			slidesToScroll: 1,
			// asNavFor: '.slider-gallery-thumbnail',
			nextArrow: '<button class="slick-next"><i class="fas fa-long-arrow-alt-right"></i></button>',
			prevArrow: '<button class="slick-prev"><i class="fas fa-long-arrow-alt-left"></i></button>',
		} );
	}
	function sanphamImageThumbnailSlider() {
		$( '.slider-gallery-thumbnail' ).slick( {
			autoplay: false,
			arrows: false,
			dots: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			asNavFor: '.slider-gallery',
			// centerMode: true,
			focusOnSelect: true
		} );
	}
	function zoomFeatureImage() {
		$('.slider-gallery').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			var img = $(slick.$slides[nextSlide]).find("img");
			$('.zoomWindowContainer,.zoomContainer').remove();
			$(img).elevateZoom({
				zoomType: "inner",
				cursor: "pointer",
			});
		});
		$('.slider-gallery .slick-current .img-zoom').elevateZoom({
			zoomType: "inner",
			// cursor: "pointer",
		});
	}

	if ( $("body").hasClass("single-product") ) {
		sanphamImageSlider();
		sanphamImageThumbnailSlider();
		zoomFeatureImage();
	}


	function tin_tuc_slider() {
		$( '.slide-post .uabb-blog-posts' ).slick( {
			autoplay: false,
			arrows: true,
			dots: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			nextArrow: '<button class="slick-next"><i class="fas fa-chevron-right"></i></button>',
			prevArrow: '<button class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
			responsive: [
							{
							  breakpoint: 500,
							  settings: {
								slidesToShow: 2,
								slidesToScroll: 1,
								infinite: true,
							  }
							},
						]
		} );
	}
	if ( $("body").hasClass("page-id-27") ) {
		tin_tuc_slider();
	}

	function toggleMobileMenu() {
		var $body = $('body'),
			mobileClass = 'mobile-menu-open',
			clickEvent = 'ontouchstart' in window ? 'touchstart' : 'click',
			transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';

		// Click to show mobile menu.
		$('.menu-toggle').on( 'click', function(e) {
			e.preventDefault();
			event.stopPropagation(); // Do not trigger click event on '.wrapper' below.

			$body.toggleClass(mobileClass);

		});

		// When mobile menu is open, click on page content will close it.
		$('.fl-page-content').on( 'click', function(event) {
			if (!$body.hasClass(mobileClass)) {
				return;
			}
			event.preventDefault();
			$body.removeClass(mobileClass);
		});

		var width = $(document).width();
		if (width <= 780) {
			// Click show sub menu.
			$( '.menu-item-has-children > a' ).on( 'click', function(e) {
				e.preventDefault();
				event.stopPropagation();
				$(this).parent().find( '.sub-menu' ).toggleClass( 'sub-menu-mobile' );
			} );
		}
	}
	toggleMobileMenu();

} );