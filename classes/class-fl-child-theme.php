<?php

/**
 * Helper class for child theme functions.
 *
 * @class FLChildTheme
 */
final class FLChildTheme {

    /**
	 * Enqueues scripts and styles.
	 *
     * @return void
     */
    static public function enqueue_scripts()
    {
	    wp_enqueue_style( 'fl-child-theme', FL_CHILD_THEME_URL . '/style.css' );
        wp_enqueue_script( 'fl_script', get_stylesheet_directory_uri() . '/js/script.js', array( 'jquery' ), '', true );

        if ( is_singular( 'product' ) || is_page( 'tin-tuc' ) ) {
           wp_enqueue_style( 'fl-child-slick', FL_CHILD_THEME_URL . '/css/slick.css' );
           wp_enqueue_script( 'fl-slick', get_stylesheet_directory_uri() . '/js/slick.js', array( 'jquery' ), '1.8.0', true );
        }

        if ( is_singular( 'product' ) ) {
            wp_enqueue_script( 'fl_elevatezoom_script', get_stylesheet_directory_uri() . '/js/jquery.elevatezoom.js', array( 'jquery' ) );
        }
    }
}